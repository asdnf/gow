Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1804"

  # config.vm.synced_folder ".", "/vagrant",
  config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__args: [
      "--verbose",
      "--rsync-path='sudo rsync'",
      "--archive",
      "--delete",
      "-z"
  ]

  # config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "forwarded_port", guest: 8080, host: 8080

  config.vm.provider "default" do |box|
    box.memory = 1024
    box.cpus = 4
  end

  config.vm.provision "bootstrap", type: "shell", inline: <<-SHELL
      apt-get update > /dev/null && echo "Updated apt"
      apt-get install -y postgresql nmap maven openjdk-8-jdk git tomcat8 libvirt-bin libvirt-dev > /dev/null && echo "Installed software"
  
      GEN_VERSION="20190410T214502Z"
      wget -c http://distfiles.gentoo.org/releases/amd64/autobuilds/$GEN_VERSION/install-amd64-minimal-$GEN_VERSION.iso \
         &> /dev/null && echo "Downloaded gentoo image"
  
      if [[ ! -f image.qcow2 ]]; then
        qemu-img create -f qcow2 image.qcow2 10G && echo "Created qcow2 image"
      fi

      echo -e "postgres\npostgres" | passwd postgres
      sed -i 's/md5/trust/g' /etc/postgresql/10/main/pg_hba.conf
      sed -i 's/peer/trust/g' /etc/postgresql/10/main/pg_hba.conf
      systemctl restart postgresql

      usermod tomcat8 -aG libvirt
      
      # export JAVA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005"
  SHELL

  config.vm.provision "clear-db", type: "shell", inline: <<-SHELL
    psql -U postgres --db postgres -c "drop database if exists gaw"
    psql -U postgres --db postgres -c "create database gaw"
  SHELL

  config.vm.provision "pivo", type: "shell", inline: <<-SHELL
    systemctl stop tomcat8
    rm -rf /var/lib/tomcat8/webapps/*
    rm -rf /var/log/tomcat8/*
    systemctl start tomcat8

    su - vagrant -c "mvn clean install -f /vagrant/pom.xml"
    cp /vagrant/service/target/service.war /var/lib/tomcat8/webapps/
    cp /vagrant/ui/target/ui.war /var/lib/tomcat8/webapps/
  SHELL

  config.vm.provision "rest", type: "shell", inline: <<-SHELL
    curl -X PUT http://localhost:8080/service/connect -d '{
      "name": "first connection ever",
      "url": "qemu:///system" 
    }' -H 'Content-Type: application/json' 2>/dev/null
    curl -X GET http://localhost:8080/service/connect 2>/dev/null
    curl -X PUT http://localhost:8080/service/connect/1/activate 2>/dev/null
    curl -X GET http://localhost:8080/service/connect/active 2>/dev/null
  SHELL

  config.vm.provision "watch", type: "shell", inline: <<-SHELL
    tail -f /var/log/tomcat8/*
  SHELL
end
