package ru.main.pavel.service.controller;

import org.libvirt.LibvirtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;
import ru.main.pavel.service.domain.ConnectionEntity;
import ru.main.pavel.service.repository.ConnectionRepository;
import ru.main.pavel.service.service.ConnectionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/connect")
public class ConnectController extends ServiceEntityController<ConnectionEntity> {

    @Autowired
    private ConnectionService connectionService;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Override
    protected JpaRepository<ConnectionEntity, Long> getRepository() {
        return connectionRepository;
    }

    @RequestMapping(value = "{id}/activate", method = RequestMethod.PUT)
    @ResponseBody
    public Long activateConnection(@PathVariable("id") Long id) throws LibvirtException {
        ConnectionEntity entity = connectionRepository.getOne(id);
        if (entity != null) {
//            String url = entity.getUrl();
            connectionService.activateConnection(id);
        }
        return entity.getId();
    }

    @RequestMapping("active")
    @ResponseBody
    public List<ConnectionEntity> getActive() {
        Set<ConnectionEntity> entitySet = connectionService.listActiveConnections().keySet();
        return new ArrayList<>(entitySet);
    }

}
