package ru.main.pavel.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.main.pavel.service.domain.ServiceEntity;

@Repository
public interface ServiceEntityRepository extends JpaRepository<ServiceEntity, Long> {
}
