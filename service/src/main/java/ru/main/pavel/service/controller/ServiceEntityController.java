package ru.main.pavel.service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;
import ru.main.pavel.service.domain.ServiceEntity;

import java.io.IOException;
import java.util.List;

public abstract class ServiceEntityController<T extends ServiceEntity> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    protected abstract JpaRepository<T, Long> getRepository();

    private T cloneWithMapper(T in) throws IOException {
        String val = MAPPER.writeValueAsString(in);
        return (T) MAPPER.readValue(val, in.getClass());
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<T> listServiceEntities() {
        return getRepository().findAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    @ResponseBody
    public Long postServiceEntity(@RequestBody T entity, @PathVariable("id") Long id) {
        if (entity != null) {
            entity.setId(id);
            getRepository().saveAndFlush(entity);
        }
        return entity.getId();
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Long putServiceEntity(@RequestBody T entity) {
        if (entity != null) {
            entity.setId(null);
            getRepository().saveAndFlush(entity);
        }
        return entity.getId();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public T deleteServiceEntity(@PathVariable("id") Long id) {
        T entity = getRepository().getOne(id);
        if (entity == null) {
            return null;
        } else {
            getRepository().deleteById(id);
            return entity;
        }
    }
}
