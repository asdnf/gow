package ru.main.pavel.service.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ConnectionEntity extends ServiceEntity {

    @Column
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
