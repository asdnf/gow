package ru.main.pavel.service.service;

import org.libvirt.Connect;
import org.libvirt.LibvirtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.main.pavel.service.domain.ConnectionEntity;
import ru.main.pavel.service.repository.ConnectionRepository;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConnectionService {

    @Autowired
    private ConnectionRepository connectionRepository;

    private Map<Long, Connect> activeConnections = new HashMap<>();

    @Transactional
    public Map<ConnectionEntity, Connect> listActiveConnections() {
        List<ConnectionEntity> entities = connectionRepository.findAll();
        Map<ConnectionEntity, Connect> result = new HashMap<>();
        for (ConnectionEntity ce : entities) {
            Connect connect = activeConnections.get(ce.getId());
            if (connect != null) {
                result.put(ce, connect);
            }
        }
        return result;
    }

    @Transactional
    public void activateConnection(Long id) throws LibvirtException {
        ConnectionEntity entity = connectionRepository.getOne(id);
        if (entity != null) {
            Connect connect = new Connect(entity.getUrl(), false);
            if (connect != null) {
                activeConnections.put(id, connect);
            }
        }
    }
}
